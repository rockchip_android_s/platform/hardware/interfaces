/*
 * Copyright (c) 2018, Fuzhou Rockchip Electronics Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "RgaCropScale.h"
#include <utils/Singleton.h>
#include <RockchipRga.h>
#include <im2d_api/im2d.h>

namespace android {
namespace camera2 {
#if !defined(TARGET_RK312X)
#define RGA_VER (2.0)
#define RGA_ACTIVE_W (4096)
#define RGA_VIRTUAL_W (4096)
#define RGA_ACTIVE_H (4096)
#define RGA_VIRTUAL_H (4096)
#else
#define RGA_VER (1.0)
#define RGA_ACTIVE_W (2048)
#define RGA_VIRTUAL_W (4096)
#define RGA_ACTIVE_H (2048)
#define RGA_VIRTUAL_H (2048)

#endif

int RgaCropScale::CropScaleNV12Or21(struct Params* in, struct Params* out)
{
    rga_info_t src, dst;
    rga_buffer_handle_t src_handle;
    rga_buffer_handle_t dst_handle;
    memset(&src, 0, sizeof(rga_info_t));
    memset(&dst, 0, sizeof(rga_info_t));

    if (!in || !out)
        return -1;

    if((out->width > RGA_VIRTUAL_W) || (out->height > RGA_VIRTUAL_H)){
        ALOGE("%s(%d): out wxh %dx%d beyond rga capability",
            __FUNCTION__, __LINE__,
            out->width, out->height);
        return -1;
    }

    if ((in->fmt != HAL_PIXEL_FORMAT_YCrCb_NV12 &&
        in->fmt != HAL_PIXEL_FORMAT_YCrCb_420_SP) ||
        (out->fmt != HAL_PIXEL_FORMAT_YCrCb_NV12 &&
        out->fmt != HAL_PIXEL_FORMAT_YCrCb_420_SP)) {
        ALOGE("%s(%d): only accept NV12 or NV21 now. in fmt %d, out fmt %d",
            __FUNCTION__, __LINE__,
            in->fmt, out->fmt);
        return -1;
    }
    RockchipRga& rkRga(RockchipRga::get());
    im_handle_param_t param;
    param.width = in->width;
    param.height = in->height;
    param.format = in->fmt;


    if (in->fd == -1) {
        src.fd = -1;
        src.virAddr = (void*)in->vir_addr;
        src_handle = importbuffer_virtualaddr(src.virAddr, &param);
        ALOGD("@%s，src virtual:%p,width:%d,height:%d",__FUNCTION__,src.virAddr,param.width,param.height);
    } else {
        src.fd = in->fd;
        src_handle = importbuffer_fd(src.fd, &param);
        ALOGD("@%s， src fd:%d,width:%d,height:%d",__FUNCTION__,src.fd,param.width,param.height);
    }
    src.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);

    param.width = out->width;
    param.height = out->height;
    param.format = out->fmt;

    if (out->fd == -1 ) {
        dst.fd = -1;
        dst.virAddr = (void*)out->vir_addr;
        dst_handle = importbuffer_virtualaddr(dst.virAddr, &param);
        ALOGD("@%s，dst virtual:%p,width:%d,height:%d",__FUNCTION__,dst.virAddr,param.width,param.height);
    } else {
        dst.fd = out->fd;
        dst_handle = importbuffer_fd(dst.fd, &param);
        ALOGD("@%s， dst fd:%d,width:%d,height:%d",__FUNCTION__,dst.fd,param.width,param.height);
    }
    dst.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);

    rga_set_rect(&src.rect,
                in->offset_x,
                in->offset_y,
                in->width,
                in->height,
                in->width_stride,
                in->height_stride,
                in->fmt);

    rga_set_rect(&dst.rect,
                out->offset_x,
                out->offset_y,
                out->width,
                out->height,
                out->width_stride,
                out->height_stride,
                out->fmt);
    if (in->mirror)
        src.rotation = DRM_RGA_TRANSFORM_FLIP_H;

    src.handle = src_handle;
    src.fd = 0;
    dst.handle = dst_handle;
    dst.fd = 0;

    if (rkRga.RkRgaBlit(&src, &dst, NULL)) {
        ALOGE("%s:rga blit failed", __FUNCTION__);
        return -1;
    }
    releasebuffer_handle(src_handle);
    releasebuffer_handle(dst_handle);
    return 0;
}

int RgaCropScale::rga_nv12_scale_crop(
		int src_width, int src_height,
		unsigned long src_fd, unsigned long dst_fd,
		int dst_width, int dst_height,
		int zoom_val, bool mirror, bool isNeedCrop,
		bool isDstNV21, bool is16Align, bool isYuyvFormat)
{
    int ret = 0;
    rga_info_t src,dst;
    int zoom_cropW,zoom_cropH;
    int ratio = 0;
    int zoom_top_offset=0,zoom_left_offset=0;
    rga_buffer_handle_t src_handle;
    rga_buffer_handle_t dst_handle;

    RockchipRga& rkRga(RockchipRga::get());

    im_handle_param_t param;
    param.width = src_width;
    param.height = src_height;
    param.format = HAL_PIXEL_FORMAT_YCrCb_NV12;

    memset(&src, 0, sizeof(rga_info_t));
    if (isYuyvFormat) {
        src.fd = -1;
        src.virAddr = (void*)src_fd;
        src_handle = importbuffer_virtualaddr(src.virAddr, &param);
        ALOGD("@%s，src virtual:%p,width:%d,height:%d",__FUNCTION__,src.virAddr,param.width,param.height);
    } else {
        src.fd = src_fd;
        src_handle = importbuffer_fd(src.fd, &param);
        ALOGD("@%s，src fd:%d,width:%d,height:%d",__FUNCTION__,src.fd,param.width,param.height);
    }
    src.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);
    memset(&dst, 0, sizeof(rga_info_t));
    dst.fd = dst_fd;
    param.width = dst_width;
    param.height = dst_height;
    if (isDstNV21){
        param.format = HAL_PIXEL_FORMAT_YCrCb_420_SP;
    }else{
        param.format = HAL_PIXEL_FORMAT_YCrCb_NV12;
    }

    dst_handle = importbuffer_fd(dst.fd, &param);
    ALOGD("@%s， dst fd:%d,width:%d,height:%d,isDstNV21:%d",__FUNCTION__,dst.fd,param.width,param.height,isDstNV21);
    dst.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);

    if((dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H)){
        ALOGE("(dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H), switch to arm ");
        ret = -1;
        goto END;
    }

    //need crop ? when cts FOV,don't crop
    if(isNeedCrop && (src_width*100/src_height) != (dst_width*100/dst_height)) {
        ratio = ((src_width*100/dst_width) >= (src_height*100/dst_height))?
                (src_height*100/dst_height):
                (src_width*100/dst_width);
        zoom_cropW = (ratio*dst_width/100) & (~0x01);
        zoom_cropH = (ratio*dst_height/100) & (~0x01);
        zoom_left_offset=((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset=((src_height-zoom_cropH)>>1) & (~0x01);
    }else{
        zoom_cropW = src_width;
        zoom_cropH = src_height;
        zoom_left_offset=0;
        zoom_top_offset=0;
    }

    if(zoom_val > 100){
        zoom_cropW = zoom_cropW*100/zoom_val & (~0x01);
        zoom_cropH = zoom_cropH*100/zoom_val & (~0x01);
        zoom_left_offset = ((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset= ((src_height-zoom_cropH)>>1) & (~0x01);
    }

    //usb camera height align to 16,the extra eight rows need to be croped.
    if(!is16Align){
        zoom_top_offset = zoom_top_offset & (~0x07);
    }

    rga_set_rect(&src.rect, zoom_left_offset, zoom_top_offset,
                zoom_cropW, zoom_cropH, src_width,
                src_height, HAL_PIXEL_FORMAT_YCrCb_NV12);
    if (isDstNV21)
        rga_set_rect(&dst.rect, 0, 0, dst_width, dst_height,
                    dst_width, dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_420_SP);
    else
        rga_set_rect(&dst.rect, 0,0,dst_width,dst_height,
                    dst_width,dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_NV12);

    if (mirror)
        src.rotation = DRM_RGA_TRANSFORM_FLIP_H;
    //TODO:sina,cosa,scale_mode,render_mode

    src.handle = src_handle;
    src.fd = 0;
    dst.handle = dst_handle;
    dst.fd = 0;
    ret = rkRga.RkRgaBlit(&src, &dst, NULL);
    if (ret) {
        ALOGE("%s:rga blit failed", __FUNCTION__);
        goto END;
    }

    END:
    releasebuffer_handle(src_handle);
    releasebuffer_handle(dst_handle);
    return ret;
}
int RgaCropScale::rga_rgb_nv12_scale_crop(
		int src_width, int src_height,
		unsigned long src_fd, unsigned long dst_fd,
		int dst_width, int dst_height,
		int zoom_val, bool mirror, bool isNeedCrop,
		bool isDstNV21, bool is16Align, bool isYuyvFormat)
{
    int ret = 0;
    rga_info_t src,dst;
    int zoom_cropW,zoom_cropH;
    int ratio = 0;
    int zoom_top_offset=0,zoom_left_offset=0;
    rga_buffer_handle_t src_handle;
    rga_buffer_handle_t dst_handle;

    RockchipRga& rkRga(RockchipRga::get());

    im_handle_param_t param;
    param.width = src_width;
    param.height = src_height;
    param.format = 0x7 << 8;

    memset(&src, 0, sizeof(rga_info_t));
    if (isYuyvFormat) {
        src.fd = -1;
        src.virAddr = (void*)src_fd;
        src_handle = importbuffer_virtualaddr(src.virAddr, &param);
        ALOGD("@%s，src virtual:%p,width:%d,height:%d",__FUNCTION__,src.virAddr,param.width,param.height);
    } else {
        src.fd = src_fd;
        src_handle = importbuffer_fd(src.fd, &param);
        ALOGD("@%s，src fd:%d,width:%d,height:%d",__FUNCTION__,src.fd,param.width,param.height);
    }
    src.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);
    memset(&dst, 0, sizeof(rga_info_t));
    dst.fd = dst_fd;
    param.width = dst_width;
    param.height = dst_height;
    if (isDstNV21){
        param.format = HAL_PIXEL_FORMAT_YCrCb_420_SP;
    }else{
        param.format = HAL_PIXEL_FORMAT_YCrCb_NV12;
    }

    dst_handle = importbuffer_fd(dst.fd, &param);
    ALOGD("@%s， dst fd:%d,width:%d,height:%d,isDstNV21:%d",__FUNCTION__,dst.fd,param.width,param.height,isDstNV21);
    dst.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);

    if((dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H)){
        ALOGE("(dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H), switch to arm ");
        ret = -1;
        goto END;
    }

    //need crop ? when cts FOV,don't crop
    if(isNeedCrop && (src_width*100/src_height) != (dst_width*100/dst_height)) {
        ratio = ((src_width*100/dst_width) >= (src_height*100/dst_height))?
                (src_height*100/dst_height):
                (src_width*100/dst_width);
        zoom_cropW = (ratio*dst_width/100) & (~0x01);
        zoom_cropH = (ratio*dst_height/100) & (~0x01);
        zoom_left_offset=((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset=((src_height-zoom_cropH)>>1) & (~0x01);
    }else{
        zoom_cropW = src_width;
        zoom_cropH = src_height;
        zoom_left_offset=0;
        zoom_top_offset=0;
    }

    if(zoom_val > 100){
        zoom_cropW = zoom_cropW*100/zoom_val & (~0x01);
        zoom_cropH = zoom_cropH*100/zoom_val & (~0x01);
        zoom_left_offset = ((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset= ((src_height-zoom_cropH)>>1) & (~0x01);
    }

    //usb camera height align to 16,the extra eight rows need to be croped.
    if(!is16Align){
        zoom_top_offset = zoom_top_offset & (~0x07);
    }

    rga_set_rect(&src.rect, zoom_left_offset, zoom_top_offset,
                zoom_cropW, zoom_cropH, src_width,
                src_height, 0x7 << 8);
    if (isDstNV21)
        rga_set_rect(&dst.rect, 0, 0, dst_width, dst_height,
                    dst_width, dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_420_SP);
    else
        rga_set_rect(&dst.rect, 0,0,dst_width,dst_height,
                    dst_width,dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_NV12);

    if (mirror)
        src.rotation = DRM_RGA_TRANSFORM_FLIP_H;
    //TODO:sina,cosa,scale_mode,render_mode

    src.handle = src_handle;
    src.fd = 0;
    dst.handle = dst_handle;
    dst.fd = 0;
    ret = rkRga.RkRgaBlit(&src, &dst, NULL);
    if (ret) {
        ALOGE("%s:rga blit failed", __FUNCTION__);
        goto END;
    }

    END:
    releasebuffer_handle(src_handle);
    releasebuffer_handle(dst_handle);
    return ret;
}


int RgaCropScale::rga_nv16_nv12_scale_crop(
		int src_width, int src_height,
		unsigned long src_fd, unsigned long dst_fd,
		int dst_width, int dst_height,
		int zoom_val, bool mirror, bool isNeedCrop,
		bool isDstNV21, bool is16Align, bool isYuyvFormat)
{
    int ret = 0;
    rga_info_t src,dst;
    int zoom_cropW,zoom_cropH;
    int ratio = 0;
    int zoom_top_offset=0,zoom_left_offset=0;
    rga_buffer_handle_t src_handle;
    rga_buffer_handle_t dst_handle;

    RockchipRga& rkRga(RockchipRga::get());

    im_handle_param_t param;
    param.width = src_width;
    param.height = src_height;
    param.format = RK_FORMAT_YCbCr_422_SP;

    memset(&src, 0, sizeof(rga_info_t));
    if (isYuyvFormat) {
        src.fd = -1;
        src.virAddr = (void*)src_fd;
        src_handle = importbuffer_virtualaddr(src.virAddr, &param);
        ALOGD("@%s，src virtual:%p,width:%d,height:%d",__FUNCTION__,src.virAddr,param.width,param.height);
    } else {
        src.fd = src_fd;
        src_handle = importbuffer_fd(src.fd, &param);
        ALOGD("@%s，src fd:%d,width:%d,height:%d",__FUNCTION__,src.fd,param.width,param.height);
    }
    src.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);
    memset(&dst, 0, sizeof(rga_info_t));
    dst.fd = dst_fd;
    param.width = dst_width;
    param.height = dst_height;
    if (isDstNV21){
        param.format = HAL_PIXEL_FORMAT_YCrCb_420_SP;
    }else{
        param.format = HAL_PIXEL_FORMAT_YCrCb_NV12;
    }

    dst_handle = importbuffer_fd(dst.fd, &param);
    ALOGD("@%s， dst fd:%d,width:%d,height:%d,isDstNV21:%d",__FUNCTION__,dst.fd,param.width,param.height,isDstNV21);
    dst.mmuFlag = ((2 & 0x3) << 4) | 1 | (1 << 8) | (1 << 10);

    if((dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H)){
        ALOGE("(dst_width > RGA_VIRTUAL_W) || (dst_height > RGA_VIRTUAL_H), switch to arm ");
        ret = -1;
        goto END;
    }

    //need crop ? when cts FOV,don't crop
    if(isNeedCrop && (src_width*100/src_height) != (dst_width*100/dst_height)) {
        ratio = ((src_width*100/dst_width) >= (src_height*100/dst_height))?
                (src_height*100/dst_height):
                (src_width*100/dst_width);
        zoom_cropW = (ratio*dst_width/100) & (~0x01);
        zoom_cropH = (ratio*dst_height/100) & (~0x01);
        zoom_left_offset=((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset=((src_height-zoom_cropH)>>1) & (~0x01);
    }else{
        zoom_cropW = src_width;
        zoom_cropH = src_height;
        zoom_left_offset=0;
        zoom_top_offset=0;
    }

    if(zoom_val > 100){
        zoom_cropW = zoom_cropW*100/zoom_val & (~0x01);
        zoom_cropH = zoom_cropH*100/zoom_val & (~0x01);
        zoom_left_offset = ((src_width-zoom_cropW)>>1) & (~0x01);
        zoom_top_offset= ((src_height-zoom_cropH)>>1) & (~0x01);
    }

    //usb camera height align to 16,the extra eight rows need to be croped.
    if(!is16Align){
        zoom_top_offset = zoom_top_offset & (~0x07);
    }

    rga_set_rect(&src.rect, zoom_left_offset, zoom_top_offset,
                zoom_cropW, zoom_cropH, src_width,
                src_height, RK_FORMAT_YCbCr_422_SP);
    if (isDstNV21)
        rga_set_rect(&dst.rect, 0, 0, dst_width, dst_height,
                    dst_width, dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_420_SP);
    else
        rga_set_rect(&dst.rect, 0,0,dst_width,dst_height,
                    dst_width,dst_height,
                    HAL_PIXEL_FORMAT_YCrCb_NV12);

    if (mirror)
        src.rotation = DRM_RGA_TRANSFORM_FLIP_H;
    //TODO:sina,cosa,scale_mode,render_mode

    src.handle = src_handle;
    src.fd = 0;
    dst.handle = dst_handle;
    dst.fd = 0;
    ret = rkRga.RkRgaBlit(&src, &dst, NULL);
    if (ret) {
        ALOGE("%s:rga blit failed", __FUNCTION__);
        goto END;
    }

    END:
    releasebuffer_handle(src_handle);
    releasebuffer_handle(dst_handle);
    return ret;
}

} /* namespace camera2 */
} /* namespace android */
